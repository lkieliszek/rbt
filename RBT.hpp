#include <iostream>
#include <string>

using namespace std;

struct node {
	int data{};
	node* left = nullptr;
	node* right = nullptr;
	node* parent = nullptr;
	string color;
};

class RBT {
private:
	node* root;

public:
	RBT() : root(nullptr) {}

	node* GetRoot() { return root; }

	void InsertNode(int input);
	void Insert_Fixup(node* toFix);

	void RemoveNode(node* parent, node* current, int input);
	void Remove(int input);
	void Delete_Fixup(node* toFix);

	node* TreeSearch(int stuff);

	void LeftRotate(node* x);
	void RightRotate(node* x);

	void PreorderTraversal(node* temp);
	void PostorderTraversal(node *temp);
};