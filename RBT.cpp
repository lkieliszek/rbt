#include "RBT.hpp"

void RBT::InsertNode(int input) {
	if (root == nullptr) {
		root = new node();
		root->data = input;
		root->parent = nullptr;
		root->color = "BLACK";
		cout << "Element inserted.\n";
	}
	else {
		auto linker = GetRoot();
		node* newnode = new node();
		newnode->data = input;

		while (linker != nullptr) {
			if (linker->data > input) {
				if (linker->left == nullptr) {
					linker->left = newnode;
					newnode->color = "RED";
					newnode->parent = linker;
					cout << "Element inserted.\n"; break;
				}
				else { linker = linker->left; }
			}
			else {
				if (linker->right == nullptr) {
					linker->right = newnode;
					newnode->color = "RED";
					newnode->parent = linker;
					cout << "Element inserted.\n"; break;
				}
				else { linker = linker->right; }
			}
		}
		Insert_Fixup(newnode);
	}
}

void RBT::Insert_Fixup(node* toFix) {
	while (toFix->parent->color == "RED") {
		auto grandparent = toFix->parent->parent;
		auto uncle = GetRoot();
		if (toFix->parent == grandparent->left) {
			if (grandparent->right) { uncle = grandparent->right; }
			if (uncle->color == "RED") {
				toFix->parent->color = "BLACK";
				uncle->color = "BLACK";
				grandparent->color = "RED";
				if (grandparent->data != root->data) { toFix = grandparent; }
				else { break; }
			}
			else if (toFix == grandparent->left->right) {
				LeftRotate(toFix->parent);
			}
			else {
				toFix->parent->color = "BLACK";
				grandparent->color = "RED";
				RightRotate(grandparent);
				if (grandparent->data != root->data) { toFix = grandparent; }
				else { break; }
			}
		}
		else {
			if (grandparent->left) { uncle = grandparent->left; }
			if (uncle->color == "RED") {
				toFix->parent->color = "BLACK";
				uncle->color = "BLACK";
				grandparent->color = "RED";
				if (grandparent->data != root->data) { toFix = grandparent; }
				else { break; }
			}
			else if (toFix == grandparent->right->left) {
				RightRotate(toFix->parent);
			}
			else {
				toFix->parent->color = "BLACK";
				grandparent->color = "RED";
				LeftRotate(grandparent);
				if (grandparent->data != root->data) { toFix = grandparent; }
				else { break; }
			}
		}
	}
	root->color = "BLACK";
}

void RBT::RemoveNode(node* parent, node* current, int input) {
	if (current == nullptr) { return; }
	if (current->data == input) {
		//CASE -- 1
		if (current->left == nullptr && current->right == nullptr) {
			if (parent->data == current->data) { root = nullptr; }
			else if (parent->right == current) {
				Delete_Fixup(current);
				parent->right = nullptr;
			}
			else {
				Delete_Fixup(current);
				parent->left = nullptr;
			}
		}
		//CASE -- 2
		else if (current->left != nullptr && current->right == nullptr) {
			int swap = current->data;
			current->data = current->left->data;
			current->left->data = swap;
			RemoveNode(current, current->right, input);
		}
		else if (current->left == nullptr && current->right != nullptr) {
			int swap = current->data;
			current->data = current->right->data;
			current->right->data = swap;
			RemoveNode(current, current->right, input);
		}
		//CASE -- 3
		else {
			bool flag = false;
			node* temp = current->right;
			while (temp->left) { flag = true; parent = temp; temp = temp->left; }
			if (!flag) { parent = current; }
			int swap = current->data;
			current->data = temp->data;
			temp->data = swap;
			RemoveNode(parent, temp, swap);
		}
	}
}

void RBT::Remove(int input) {
	auto temp = root;
	auto parent = temp;
	bool flag = false;
	if (!temp) { RemoveNode(nullptr, nullptr, input); }

	while (temp) {
		if (input == temp->data) { flag = true; RemoveNode(parent, temp, input); break; }
		else if (input < temp->data) { parent = temp; temp = temp->left; }
		else { parent = temp; temp = temp->right; }
	}

	if (!flag) { cout << "\nElement doesn't exist in the table"; }
}

void RBT::Delete_Fixup(node* toFix) {
	while (toFix->data != root->data && toFix->color == "BLACK") {
		auto sibling = GetRoot();
		if (toFix->parent->left == toFix) {
			if (toFix->parent->right) { sibling = toFix->parent->right; }
			if (sibling) {
				//CASE -- 1
				if (sibling->color == "RED") {
					sibling->color = "BLACK";
					toFix->parent->color = "RED";
					LeftRotate(toFix->parent);
					sibling = toFix->parent->right;
				}
				//CASE -- 2
				if (sibling->left == nullptr && sibling->right == nullptr) {
					sibling->color = "RED";
					toFix = toFix->parent;
				}
				else if (sibling->left->color == "BLACK" && sibling->right->color == "BLACK") {
					sibling->color = "RED";
					toFix = toFix->parent;
				}
				//CASE -- 3
				else if (sibling->right->color == "BLACK") {
					sibling->left->color = "BLACK";
					sibling->color = "RED";
					RightRotate(sibling);
					sibling = toFix->parent->right;
				}
				else {
					sibling->color = toFix->parent->color;
					toFix->parent->color = "BLACK";
					if (sibling->right) { sibling->right->color = "BLACK"; }
					LeftRotate(toFix->parent);
					toFix = root;
				}
			}
		}
		else {
			if (toFix->parent->right == toFix) {
				if (toFix->parent->left) { sibling = toFix->parent->left; }
				if (sibling) {
					//CASE -- 1
					if (sibling->color == "RED") {
						sibling->color = "BLACK";
						toFix->parent->color = "RED";
						RightRotate(toFix->parent);
						sibling = toFix->parent->left;
					}
					//CASE -- 2
					if (sibling->left == nullptr && sibling->right == nullptr) {
						sibling->color = "RED";
						toFix = toFix->parent;
					}
					else if (sibling->left->color == "BLACK" && sibling->right->color == "BLACK") {
						sibling->color = "RED";
						toFix = toFix->parent;
					}
					//CASE -- 3 
					else if (sibling->left->color == "BLACK") {
						sibling->right->color = "BLACK";
						sibling->color = "RED";
						RightRotate(sibling);
						sibling = toFix->parent->left;
					}
					else {
						sibling->color = toFix->parent->color;
						toFix->parent->color = "BLACK";
						if (sibling->left) { sibling->left->color = "BLACK"; }
						LeftRotate(toFix->parent);
						toFix = root;
					}
				}
			}

		}
	}
	toFix->color = "BLACK";
}

node* RBT::TreeSearch(int stuff) {
	auto temp = GetRoot();
	if (temp == nullptr) { return nullptr; }

	while (temp) {
		if (stuff == temp->data) { return temp; }
		else if (stuff < temp->data) { temp = temp->left; }
		else { temp = temp->right; }
	}
	return nullptr;
}

void RBT::LeftRotate(node* x) {
	node* nw_node = new node();
	if (x->right->left) { nw_node->right = x->right->left; }
	nw_node->left = x->left;
	nw_node->data = x->data;
	nw_node->color = x->color;
	x->data = x->right->data;

	x->left = nw_node;
	if (nw_node->left) { nw_node->left->parent = nw_node; }
	if (nw_node->right) { nw_node->right->parent = nw_node; }
	nw_node->parent = x;

	if (x->right->right) { x->right = x->right->right; }
	else { x->right = nullptr; }

	if (x->right) { x->right->parent = x; }
}

void RBT::RightRotate(node* x) {
	node* nw_node = new node();
	if (x->left->right) { nw_node->left = x->left->right; }
	nw_node->right = x->right;
	nw_node->data = x->data;
	nw_node->color = x->color;

	x->data = x->left->data;
	x->color = x->left->color;

	x->right = nw_node;
	if (nw_node->left) { nw_node->left->parent = nw_node; }
	if (nw_node->right) { nw_node->right->parent = nw_node; }
	nw_node->parent = x;

	if (x->left->left) { x->left = x->left->left; }
	else { x->left = nullptr; }

	if (x->left) { x->left->parent = x; }
}

void RBT::PreorderTraversal(node* temp) {
	if (!temp) { return; }
	cout << "--> " << temp->data << "<" << temp->color << ">";
	PreorderTraversal(temp->left);
	PreorderTraversal(temp->right);
}

void RBT::PostorderTraversal(node *temp) {
	if (!temp) { return; }
	PostorderTraversal(temp->left);
	PostorderTraversal(temp->right);
	cout << "--> " << temp->data << "<" << temp->color << ">";
}