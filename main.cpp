#include <iostream>
#include <string>
#include "RBT.hpp"

void menu() {
	cout << "---------------------------------------------";
	cout << "\n1. Insert Element.";
	cout << "\n2. Remove Element.";
	cout << "\n3. Search for Element.";
	cout << "\n4. PRE-ORDER Tree-Walk.";
	cout << "\n5. POST-ORDER Tree-Walk.";
	cout << "\n6. Exit.";
	cout << "\n---------------------------------------------";
	cout << "\nInput: ";
}

int main() {
	RBT demo;
	int info, input;
	menu();
	cin >> info;
	while (info != 6) {
		switch (info) {
		case 1: cout << "\nElement to be inserted: ";
			cin >> input; demo.InsertNode(input);
			break;

		case 2: cout << "\nElement to be deleted: ";
			cin >> input;
			demo.Remove(input);
			break;

		case 3: cout << "\nElement to be searched: ";
			cin >> input;
			if (demo.TreeSearch(input)) { cout << "Element found.\n"; }
			else { cout << "Element not found.\n"; }
			break;

		case 4: cout << "Pre-Order Tree Walk.";
			demo.PreorderTraversal(demo.GetRoot());
			cout << endl;
			break;

		case 5: cout << "Post-Order Tree Walk.";
			demo.PostorderTraversal(demo.GetRoot());
			cout << endl;
			break;

		default: cout << "Invalid Input.\n";
		}
		cout << "\nInput: ";
		cin >> info;
	}
	cout << "\nTerminating... ";
	return 0;
}